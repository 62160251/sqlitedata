/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jettarin.sqlteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NITRO 5
 */
public class InsertUser {
     public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            try {
                conn = DriverManager.getConnection("jdbc:sqlite:user.db");
                conn.setAutoCommit(false);
              stmt = conn.createStatement();
              String sql = "INSERT INTO COMPANY (\n" +
 "                        ID,\n"
                        + "                        NAME,\n"
                        + "                        AGE,\n"
                        + "                        ADDRESS,\n"
                        + "                        SALARY\n"
                        + "                    )\n"
                        + "                    VALUES (\n"
                        + "                        2,\n"
                        + "                        'Junpen',\n"
                        + "                        58,\n"
                        + "                        'Thailand',\n"
                        + "                        2000000.00\n"
                        + "                    );";
              
               sql = "INSERT INTO COMPANY (\n" +
 "                        ID,\n"
                        + "                        NAME,\n"
                        + "                        AGE,\n"
                        + "                        ADDRESS,\n"
                        + "                        SALARY\n"
                        + "                    )\n"
                        + "                    VALUES (\n"
                        + "                        3,\n"
                        + "                        'Jettarin',\n"
                        + "                        41,\n"
                        + "                        'Thailand',\n"
                        + "                        20.00\n"
                        + "                    );";
                stmt.executeUpdate(sql);
                stmt.close();
                conn.commit();;
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
